<?php
    require_once 'action.php';

    $result = mysqli_query($link, 'SELECT title, year, description FROM films WHERE isActive = 1');
    $cnt = mysqli_num_rows($result);
    mysqli_close($link);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="container">
        <div class="row">
            <?php
                if (!$cnt) echo '<h1>В базе нету записей</h1>';
                else { echo <<<heredoc
                        <div class="col-xs-6 col-md-4"><h3>Название фильма</h3></div>
                        <div class="col-xs-6 col-md-4"><h3>Год</h3></div>
                        <div class="col-xs-6 col-md-4"><h3>Описание</h3></div>
heredoc;
                }

                while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                    if($row) {
            ?>
            <div class="col-xs-6 col-md-4"><p><?= $row['title']; ?></p></div>
            <div class="col-xs-6 col-md-4"><p><?= $row['year']; ?></p></div>
            <div class="col-xs-6 col-md-4"><p><?= $row['description'] ? $row['description'] : 'Нету описания';?></p></div>
            <?php
                    }
                }
            ?>
            <div class="col-xs-6 col-md-4">
                <a href="index.php">Вернутся к форме</a>
            </div>
        </div>
    </div>
</body>
</html>