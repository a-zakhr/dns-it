<?php
require_once 'db_connect.php';

function clean($value = "") {
    $value = trim(stripslashes(strip_tags(htmlspecialchars(($value)))));
    return $value;
}


$title = isset($_POST['title']) ? $_POST['title'] : null;
$year = isset($_POST['year']) ? $_POST['year'] : null;
$description = isset($_POST['description']) ? $_POST['description'] : null;
$isActive = isset($_POST['isActive']) ? 1 : 0;

$title = clean($title);
$year = clean($year);
$description = clean($description);

if (!empty($title) && !empty($year)) {
    $insert = mysqli_query($link, "INSERT INTO films (title, year, isActive, description) VALUES ('$title', '$year', '$isActive', '$description')");
    mysqli_close($link);
    header('Location: index.php');
    exit;
}