<!DOCTYPE html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="table">
    <div class="table-cell">
        <div class="container">
            <div class="row">

                <h1>Хотите добавить фильм? Вперед! :)</h1>

                <div class="col-xs-6 col-md-6 col-md-offset-3">
                    <form class="form-horizontal" onsubmit="return checkForm(this)" method="post" action="action.php" autocomplete="off">

                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">Название фильма</label><span class="req">*</span>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="title" name="title">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="year" class="col-sm-2 control-label">Год премьеры</label><span class="req">*</span>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="year" name="year">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label">Описание фильма</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" id="description" name="description" rows="3"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="isActive"> Активная запись?
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Добавить</button>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <a href="output.php">Просмотреть записи</a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function checkForm(form){

        function isNumeric(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }

        var input_title = document.querySelector('div.form-group:first-child');
        var input_year = document.querySelector('div.form-group:nth-child(2)');
        var title = document.getElementById('title');
        var year = document.getElementById('year');
        var exp = /^[a-zA-Zа-яА-Я]/;


        if (title.value == '' || title.value.match(exp) == null) {
            input_title.className = 'form-group has-error';
            title.placeholder = 'Поле не заполнено';
            return false;
        } else {
            input_title.className = 'form-group';
            title.placeholder = '';
        }

        if (!isNumeric(year.value)) {
            input_year.className = 'form-group has-error';
            year.placeholder = 'Ошибка: пустое поле или не числовое значение';
            return false;
        } else {
            input_year.className = 'form-group';
            year.placeholder = '';
        }
        return true;
    }
</script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>